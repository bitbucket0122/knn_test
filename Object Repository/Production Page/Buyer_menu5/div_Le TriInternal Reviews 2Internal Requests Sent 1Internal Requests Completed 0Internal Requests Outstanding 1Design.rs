<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Le TriInternal Reviews 2Internal Requests Sent 1Internal Requests Completed 0Internal Requests Outstanding 1Design</name>
   <tag></tag>
   <elementGuidId>aa5309d3-486b-44be-8a74-06e31a91b3bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='quicktabs-tabpage-buyer_demo-2']/div/div[3]/div/ul/li[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>supplier-review</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
   
		Le TriInternal Reviews: 2Internal Requests Sent: 1Internal Requests Completed: 0Internal Requests Outstanding: 1
		Design
	
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;quicktabs-tabpage-buyer_demo-2&quot;)/div[@class=&quot;view view-buyer-employees view-id-buyer_employees view-display-id-block view-dom-id-fed87ae6552fa422d2faa1bc0d97561a jquery-once-2-processed&quot;]/div[@class=&quot;view-content&quot;]/div[@class=&quot;item-list&quot;]/ul[1]/li[@class=&quot;list-spteaser list-employe&quot;]/div[@class=&quot;supplier-review&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='quicktabs-tabpage-buyer_demo-2']/div/div[3]/div/ul/li[2]/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Design'])[2]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//li[2]/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
