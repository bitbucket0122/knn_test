<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Le TriInternal Reviews 42Internal Requests Sent 0Internal Requests Completed 0Internal Requests Outstanding 0</name>
   <tag></tag>
   <elementGuidId>92ac1025-a49a-4a6f-851b-8f10afd35d74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='quicktabs-tabpage-buyer_demo-2']/div/div[3]/div/ul/li/div[2]/div/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Le TriInternal Reviews: 42Internal Requests Sent: 0Internal Requests Completed: 0Internal Requests Outstanding: 0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;quicktabs-tabpage-buyer_demo-2&quot;)/div[@class=&quot;view view-buyer-employees view-id-buyer_employees view-display-id-block view-dom-id-f1b14a396153a75c2440786cb4bf45d5 jquery-once-2-processed&quot;]/div[@class=&quot;view-content&quot;]/div[@class=&quot;item-list&quot;]/ul[1]/li[@class=&quot;list-spteaser list-employe&quot;]/div[@class=&quot;supplier-review&quot;]/div[@class=&quot;sr_detail&quot;]/h3[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='quicktabs-tabpage-buyer_demo-2']/div/div[3]/div/ul/li/div[2]/div/h3</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order'])[2]/following::h3[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[2]/following::h3[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/ul/li/div[2]/div/h3</value>
   </webElementXpaths>
</WebElementEntity>
