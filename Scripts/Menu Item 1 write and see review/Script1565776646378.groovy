import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Supplier Performance Review Software SupplyHive/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_User account  SupplyHive/input__name'), 
    'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/find end review (chormefix)/Page_User account  SupplyHive/input__pass'), 
    'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_User account  SupplyHive/input__op'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/img'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Landing Page  SupplyHive/img_Top Picks_img_bgr'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_WP_PS_LT  SupplyHive/input_WP_PS_title'), 
    'Psy')

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_WP_PS_LT  SupplyHive/input_Psy_T_title'), 
    'Psy_T')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_WP_PS_LT  SupplyHive/a_Psy_T'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Psy_T  SupplyHive/a_Write an Internal Review'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    'product')

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/input_this is the product question_field_review_productund'), 
    'product')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_Give it 1010'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/textarea__field_review_descriptionund0value'), 
    'good')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/label_5 years'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/textarea_What do you like best about this supplier_field_about_supplierund0value'), 
    'this is a demo review')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/textarea_What do you dislike_field_you_dislikeund0value'), 
    'nothing')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/textarea_Recommendations to others considering this supplier_field_considering_supplierund0value'), 
    'Nothing')

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Create Review  SupplyHive/a_DONE'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Review submitted  SupplyHive/a_See my review'))

WebUI.selectOptionByValue(findTestObject('Object Repository/find end review (chormefix)/Page_Psy_T  SupplyHive/select_-- Select Survey -- glaBmeyer_company'), 
    '376988', true)

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Psy_T  SupplyHive/div_Me'))

WebUI.click(findTestObject('Object Repository/find end review (chormefix)/Page_Psy_T  SupplyHive/a_Log Out'))

WebUI.closeBrowser()

