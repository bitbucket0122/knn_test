import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Buyer Launchpd/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Buyer Launchpd/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/input__op'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Top Picks_img_bgr'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Top Picks_img_bgr_1'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_WP_PS_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_WP_PS_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Recommended_img_bgr_1'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_WP_PS_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Recommended_img_bgr_2'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_WP_PS_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Recommended_img_bgr_3'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_WP_PS_top_landing_logo (1)'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_Recommended_img_bgr_4'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/img_WP_PS_top_landing_logo (2)'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/div_Me'))

WebUI.click(findTestObject('Object Repository/Buyer Launchpd/a_Log Out'))

