import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/Supplier Review/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Supplier Review/input__name'), 'lesupplier1@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Supplier Review/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Supplier Review/input__op'))

WebUI.setText(findTestObject('Object Repository/Supplier Review/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    'serrvice')

WebUI.click(findTestObject('Object Repository/Supplier Review/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Give it 1010'))

WebUI.setText(findTestObject('Object Repository/Supplier Review/textarea__field_review_descriptionund0value'), 'Good')

WebUI.click(findTestObject('Object Repository/Supplier Review/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/Supplier Review/label_1-3 years'))

WebUI.setText(findTestObject('Object Repository/Supplier Review/textarea_What do you like best about this supplier_field_about_supplierund0value'), 
    'test')

WebUI.click(findTestObject('Object Repository/Supplier Review/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/Supplier Review/textarea_What do you dislike_field_you_dislikeund0value'), 
    'nothing')

WebUI.click(findTestObject('Object Repository/Supplier Review/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/Supplier Review/textarea_Recommendations to others considering this supplier_field_considering_supplierund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/Supplier Review/a_DONE'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_See my review'))

WebUI.click(findTestObject('Object Repository/Supplier Review/div_Me'))

WebUI.click(findTestObject('Object Repository/Supplier Review/a_Log Out'))

WebUI.closeBrowser()

