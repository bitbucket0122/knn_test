import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Buyer_menu_5/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Buyer_menu_5/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/img'))

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/img_Recommended_img_bgr'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Buyer_menu_5/select_zLast Name (Asc)Last Name (Desc)'), 'field_last_name_value DESC', 
    true)

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/h3_Le TriInternal Reviews 42Internal Requests Sent 0Internal Requests Completed 0Internal Requests Outstanding 0'))

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/img (1)'))

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/span_Internal Requests Outstanding_show_emoji'))

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/div_Me'))

WebUI.click(findTestObject('Object Repository/Buyer_menu_5/a_Log Out'))

WebUI.closeBrowser()

