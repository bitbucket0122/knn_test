import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/div_Abbott suppliers click here to register_landing_banner_img'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Supplier_Knnected/input__name'), 'supplier_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Supplier_Knnected/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/input__op'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/a_Email This Profile'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/a_Get Knnected with Buyers'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/img_Buyer_Test_buyers-img'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/input_Buyer_Test_op'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/div_Me'))

WebUI.click(findTestObject('Object Repository/Supplier_Knnected/a_Log Out'))

WebUI.closeBrowser()

