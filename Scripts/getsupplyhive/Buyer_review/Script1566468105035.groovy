import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/Buyer_review/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/input__field_supplier_reviewund0nid'), 'G')

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/input_Gerimis_field_supplier_reviewund0nid'), 
    'Gerimis')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/div_Gerimis'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    'product')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Give it 1010'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/textarea__field_review_descriptionund0value'), 
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Give it 35'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/label_3-5 years'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/textarea_What do you like best about this supplier_field_about_supplierund0value'), 
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/textarea_What do you dislike_field_you_dislikeund0value'), 
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_review/textarea_Recommendations to others considering this supplier_field_considering_supplierund0value'), 
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_DONE'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_See my review'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_review/a_Log Out'))

