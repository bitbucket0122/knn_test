import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/buyer_menu2/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/buyer_menu2/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/img_Top Picks_img_bgr'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Psycray_T Employees'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Supplier Scores'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Supplier Showcase'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Psycray_T Suppliers'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Production Page/buyer_menu2/select_Total Internal ReviewsTotal Public ReviewsAlphabetical (Asc)Alphabetical (Desc)Rating (Highest to Lowest)Rating (Lowest to Highest)'), 
    'publicReviews DESC', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Production Page/buyer_menu2/select_Total Internal ReviewsTotal Public ReviewsAlphabetical (Asc)Alphabetical (Desc)Rating (Highest to Lowest)Rating (Lowest to Highest)'), 
    'title ASC', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Production Page/buyer_menu2/select_Total Internal ReviewsTotal Public ReviewsAlphabetical (Asc)Alphabetical (Desc)Rating (Highest to Lowest)Rating (Lowest to Highest)'), 
    'title DESC', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Production Page/buyer_menu2/select_Total Internal ReviewsTotal Public ReviewsAlphabetical (Asc)Alphabetical (Desc)Rating (Highest to Lowest)Rating (Lowest to Highest)'), 
    'value DESC', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Production Page/buyer_menu2/select_Total Internal ReviewsTotal Public ReviewsAlphabetical (Asc)Alphabetical (Desc)Rating (Highest to Lowest)Rating (Lowest to Highest)'), 
    'value ASC', true)

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_show more'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Gerimis'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Internal Reviews'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Insights Report'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Trends'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Public Reviews'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Vendor Overview'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu2/a_Log Out'))

WebUI.closeBrowser()

