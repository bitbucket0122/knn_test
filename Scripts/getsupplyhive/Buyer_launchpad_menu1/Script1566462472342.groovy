import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu_1/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/Buyer_menu_1/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/img_Top Picks_img_bgr'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu_1/input__field_supplier_reviewund0nid'), 'Psy')

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu_1/input_Psycray_T1_field_supplier_reviewund0nid'), 
    'Psycray_T1')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/div_Psycray_T1'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu_1/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    'product')

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu_1/input_Promotional products - shirts_field_review_productund'), 
    'product')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Give it 910'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu_1/textarea__field_review_descriptionund0value'), 
    'Good')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/label_1-3 years'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_DONE'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_See my review'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu_1/a_Log Out'))

