import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_Launchpad/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/Buyer_Launchpad/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Top Picks_img_bgr'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Top Picks_img_bgr_1'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Recommended_img_bgr_1'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Recommended_img_bgr_2'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Recommended_img_bgr_3'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Recommended_img_bgr_4'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo (1)'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Recommended_img_bgr_5'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/img_Psy_top_landing_logo (2)'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_Launchpad/a_Log Out'))

WebUI.closeBrowser()

