import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/Buyer_menu3/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/Buyer_menu3/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/input__op'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_Psycray_T Suppliers'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_Psycray_T Employees'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_Supplier Scores'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_SRM Insights'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_Supplier Showcase'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/Buyer_menu3/a_Log Out'))

