import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/buyer_menu4/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/buyer_menu4/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Overall Score'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Overall Score 90-100 7'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Overall Score 70-89 4'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Overall Score 69 or below 21'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Empty Overall Score 5'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Reputation Score'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Reputation Score'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Reputation Score'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Reputation Score 90-100 9'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Reputation Score 70-89 12'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/li_Reputation Score 69 or below 1'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Sort By'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Sort By'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu4/a_Log Out'))

