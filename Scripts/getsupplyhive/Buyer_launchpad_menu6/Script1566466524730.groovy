import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Production Page/buyer_menu6/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Production Page/buyer_menu6/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/input__op'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Main Menu'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Lets Make a Match'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_The Applicants'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_yes'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Supplier_t'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Internal Reviews'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Insights Report'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Trends'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Public Reviews'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Vendor Overview'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/div_Me'))

WebUI.click(findTestObject('Object Repository/Production Page/buyer_menu6/a_Log Out'))

WebUI.closeBrowser()

