import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getsupplyhive.com/')

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_LOG IN'))

WebUI.setText(findTestObject('Production Page/Buyer_menu5/input__name'), 'lebuyer@gmail.com')

WebUI.setEncryptedText(findTestObject('Production Page/Buyer_menu5/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_Main Menu'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/img_Recommended_img_bgr'))

WebUI.selectOptionByValue(findTestObject('Production Page/Buyer_menu5/select_zLast Name (Asc)Last Name (Desc)'), 
    'field_last_name_value DESC', true)

WebUI.click(findTestObject('Production Page/Buyer_menu5/span_Internal Requests Outstanding_show_emoji'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/span_Internal Requests Outstanding_show_emoji'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/div_Le TriInternal Reviews 3Internal Requests Sent 0Internal Requests Completed 0Internal Requests Outstanding 0Design'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/div_Le TriInternal Reviews 2Internal Requests Sent 1Internal Requests Completed 0Internal Requests Outstanding 1Design'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_Supplier Scores'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_SRM Insights'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_Supplier Showcase'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_Psycray_T Suppliers'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/div_Me'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/div_Me'))

WebUI.click(findTestObject('Production Page/Buyer_menu5/a_Log Out'))

