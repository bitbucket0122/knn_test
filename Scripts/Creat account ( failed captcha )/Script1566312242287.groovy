import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Page_Supplier Performance Review Software SupplyHive/a_LOG IN'))

WebUI.click(findTestObject('Page_User account  SupplyHive/a_Create new account'))

WebUI.setText(findTestObject('Page_User account  SupplyHive/input__mail'), 'newaccount@demo1.com')

WebUI.setEncryptedText(findTestObject('Page_User account  SupplyHive/input__passpass1'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.setEncryptedText(findTestObject('Page_User account  SupplyHive/input__passpass2'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Page_User account  SupplyHive/body_recaptchaanchorMaininit(x22ainputx22x22bgdatax22x22Ly93d3cuZ29vZ2xlLmNvbS9qcy9iZy81RmR0N21pOU9LdDc1cEJubmhJZE5KcEs2ZlU3aGJJOWtQdDBfNXpoNHE4Lmpzx22x22x22x22RExnZ09pOEZlMUYyMU1jdzh4eGQzNDgzTXZXbmpPWE5SQmdoUXRLUmNCZ3A4'))

WebUI.click(findTestObject('Object Repository/Creat account/img_Click verify once there are none left_rc-image-tile-33'))

WebUI.click(findTestObject('Object Repository/Creat account/img_Click verify once there are none left_rc-image-tile-33'))

WebUI.click(findTestObject('Object Repository/Creat account/img_Click verify once there are none left_rc-image-tile-33'))

WebUI.click(findTestObject('Object Repository/Creat account/div_Click verify once there are none left_rc-image-tile-overlay'))

WebUI.click(findTestObject('Object Repository/Creat account/button_Verify'))

WebUI.closeBrowser()

