import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/buyer_menu 6/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/input__op'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Lets Make a Match'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_The Applicants'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_The Hunt'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Advertising - Media Buy'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Filter'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Filter'))

WebUI.selectOptionByValue(findTestObject('Object Repository/buyer_menu 6/select_Total Internal ReviewsTotal Public ReviewsAlphabetical (Asc)Alphabetical (Desc)Rating (Highest to Lowest)Rating (Lowest to Highest)'), 
    'publicReviews DESC', true)

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Read more (1)'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_WP_PS_top_landing_logo (1)'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Lets Make a Match'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Advertising - Media Buy'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_WP_PS_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_Recommended_img_bgr'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_The Applicants'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/span_yes'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Read more'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_WP_PS_top_landing_logo (1)'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/img_Recommended_img_bgr'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/input_WP_PS_title'), 'Psy')

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/input_psyctest_title'), 'Psy_T')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Psy_T'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Write an Internal Review'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    '.')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Give it 1010'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/textarea__field_review_descriptionund0value'), '.')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Give it 45'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/label_1-3 years'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/textarea_What do you like best about this supplier_field_about_supplierund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/textarea_What do you dislike_field_you_dislikeund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/buyer_menu 6/textarea_Recommendations to others considering this supplier_field_considering_supplierund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_DONE'))

WebUI.click(findTestObject('Object Repository/buyer_menu 6/a_Log Out'))

WebUI.closeBrowser()

