import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Buyer_lp1_review/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/input__op'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/img'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/img_Top Picks_img_bgr'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/input__field_supplier_reviewund0nid'), 'Ps')

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/input_Psy_T_field_supplier_reviewund0nid'), 'Psy_T')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/div_Psy_T'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    'product')

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/input_this is the product question_field_review_productund'), 
    'product')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_Give it 1010'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/textarea__field_review_descriptionund0value'), 'good')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_Give it 55'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/div_Less than 1 year    1-3 years    3-5 years    5 years'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/label_1-3 years'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/textarea_What do you like best about this supplier_field_about_supplierund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/textarea_What do you dislike_field_you_dislikeund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_This is a required question'))

WebUI.setText(findTestObject('Object Repository/Buyer_lp1_review/textarea_Recommendations to others considering this supplier_field_considering_supplierund0value'), 
    '.')

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_DONE'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/a_See my review'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Buyer_lp1_review/select_-- Select Survey -- glaBmeyer_company'), 
    '376988', true)

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/div_Me'))

WebUI.click(findTestObject('Object Repository/Buyer_lp1_review/div_Me'))

