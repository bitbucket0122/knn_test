import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/supplier_action plan/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input__name'), 'lesupplier1@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/supplier_action plan/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/supplier_action plan/input__op'))

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input_glaBmeyer_company_title'), 'gla')

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input_GLAUCOMA  REFRACTIVE INTERNATIONAL_title'), 'glaBmeyer_company')

WebUI.click(findTestObject('Object Repository/supplier_action plan/a_glaBmeyer_company'))

WebUI.click(findTestObject('Object Repository/supplier_action plan/a_Action Plans'))

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input_Where can I get some_name'), 'Why do we use it?')

WebUI.click(findTestObject('Object Repository/supplier_action plan/a_1'))

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input_Action Steps_steps'), 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ')

WebUI.setText(findTestObject('Object Repository/supplier_action plan/textarea_How to Document and Track Progress_body'), 
    'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.')

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input_Signatures and Dates_signatures1firstName'), 
    'Le')

WebUI.setText(findTestObject('Object Repository/supplier_action plan/input_Signatures and Dates_signatures1lastName'), 'Tri')

WebUI.click(findTestObject('Object Repository/supplier_action plan/a_1'))

WebUI.click(findTestObject('Object Repository/supplier_action plan/input_Signatures and Dates_save'))

WebUI.click(findTestObject('Object Repository/supplier_action plan/div_Me'))

WebUI.click(findTestObject('Object Repository/supplier_action plan/a_Log Out'))

