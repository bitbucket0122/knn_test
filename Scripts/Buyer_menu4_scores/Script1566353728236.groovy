import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Page_Supplier Performance Review Software SupplyHive/a_LOG IN'))

WebUI.setText(findTestObject('Page_User account  SupplyHive/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Page_User account  SupplyHive/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Page_User account  SupplyHive/input__op'))

WebUI.click(findTestObject('Page_Create Review  SupplyHive/img'))

WebUI.click(findTestObject('Page_Landing Page  SupplyHive/img_Recommended_img_bgr'))

WebUI.selectOptionByValue(findTestObject('Page_WP_PS_LT  SupplyHive/select_-- Select Survey -- glaBmeyer_company'), '376988', 
    true)

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Overall Score'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Overall Score 90-100 7'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Overall Score 70-89 4'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Overall Score 69 or below 21'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Empty Overall Score 5'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Reputation Score'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Reputation Score'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Reputation Score 90-100 9'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Reputation Score 70-89 12'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Reputation Score 69 or below 1'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/li_Empty Reputation Score 35'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Reputation Score'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Sort By'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Sort By'))

WebUI.click(findTestObject('Object Repository/Buyer_menu4/li_Supplier Name'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Sort By'))

WebUI.click(findTestObject('Object Repository/Buyer_menu4/li_Overall Score'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Sort By'))

WebUI.click(findTestObject('Object Repository/Buyer_menu4/li_Reputation Score'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Sort By'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Sort By'))

WebUI.click(findTestObject('Object Repository/Buyer_menu4/li_Spend'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/div_Me'))

WebUI.click(findTestObject('Page_WP_PS_LT  SupplyHive/a_Log Out'))

