import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/buyer_menu2/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/buyer_menu2/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/buyer_menu2/input__op'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/img_WP_PS_top_landing_logo'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/img_Top Picks_img_bgr'))

WebUI.setText(findTestObject('Object Repository/buyer_menu2/input_WP_PS_title'), 'gla')

WebUI.setText(findTestObject('Object Repository/buyer_menu2/input_glaBmeyer_company_title'), 'glaBmeyer_company')

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_glaBmeyer_company'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Internal Reviews'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Insights Report'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Trends'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Public Reviews'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Write an Internal Review'))

WebUI.setText(findTestObject('Object Repository/buyer_menu2/input_Enter productsservices separated by a comma Example paper products office supplies_field_review_productund'), 
    'service')

WebUI.setText(findTestObject('Object Repository/buyer_menu2/input_Print Service_field_review_productund'), 'service')

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_This is a required question'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Give it 910'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Give it 35'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_DONE'))

WebUI.click(findTestObject('Object Repository/buyer_menu2/a_Log Out'))

WebUI.closeBrowser()

