import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://knn.psycray.com/')

WebUI.click(findTestObject('Object Repository/buyer_menu3/a_LOG IN'))

WebUI.setText(findTestObject('Object Repository/buyer_menu3/input__name'), 'buyer_le@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/buyer_menu3/input__pass'), 'Mw5MzQ6RuvDjqqZ+h09SXw==')

WebUI.click(findTestObject('Object Repository/buyer_menu3/input__op'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/img'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/img_Recommended_img_bgr'))

WebUI.selectOptionByValue(findTestObject('Object Repository/buyer_menu3/select_-- Select Survey -- glaBmeyer_company'), 
    '376988', true)

WebUI.click(findTestObject('Object Repository/buyer_menu3/a_WP_PS_LT Suppliers'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/a_WP_PS_LT Employees'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/a_Supplier Scores'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/a_SRM Insights'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/i_Me_angle-down'))

WebUI.click(findTestObject('Object Repository/buyer_menu3/a_Log Out'))

WebUI.closeBrowser()

